server code: https://gitlab.com/riderside/xarxacanmata-server

	You need a micropython firmware on your device.
	Code has been tested on 1.19.1

	Rename file config-base.json to config.json.
	Set your WiFI parameters in config.json.
	Upload to your device the following files:
	main.py
	boot.py
	bme680.py
	config_reader.py
	blinker.py
	config.json

	Once you've uploaded the files your must restart your device.
	If your device blinks 4 times it's connected to your WiFi network
	and it will start collecting data each 5 minutes.

	For more info about blink codes see at the end of this file in secction "Blink codes"


	Blink codes:
	3 times: Failed to read config file.
	4 times: Connected to WiFi
	5 times: Failed to sync with time server. System will try 3 times waitting
		    10s between each try.
	6 times: Failed to setup BME680 sensor. Check your wires.
	7 times: Failed to read BME680 sensor data. System will try 3 times waitting
		    10s between each try and reset.
	3 times: Failed to read config file.
