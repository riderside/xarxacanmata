# This file is executed on every boot (including wake-boot from deepsleep)
import esp
esp.osdebug(None)
import gc
gc.collect()
import machine
import ubinascii
import network
import config_reader as configReader
from blinker import blink

MAC= None

def getMAC(raw_MAC):
    global MAC
    MAC = ubinascii.hexlify(raw_MAC).decode().upper()

def doConnect():
    sta_if = network.WLAN(network.STA_IF) #WiFi Client
    ap_if = network.WLAN(network.AP_IF) #WiFi AP
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(config['WIFI_SSID'], config['WIFI_PASSWORD'])
        while not sta_if.isconnected():
            machine.idle()
    getMAC(sta_if.config('mac'))
    print('network config:', sta_if.ifconfig())
    ap_if.active(False) # disable AP
    blink(4, 0.250)
    gc.collect()
try:
    config = configReader.readConfig()    
except Exception as e:
    print('Failed to read config Resetting...', e)
    blink(3, 0.250)
doConnect()