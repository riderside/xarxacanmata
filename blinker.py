def blink(iterations, pause):
    from machine import Signal, Pin
    from time import sleep
    pin = Pin(2, Pin.OUT)
    led = Signal(pin, invert=True)
    led.off()
    for i in range(0, iterations):
        sleep(pause)
        led.on()
        sleep(pause)
        led.off()
    sleep(2)
    led.off()