# Complete project details at https://RandomNerdTutorials.com/micropython-bme680-esp32-esp8266/
import machine
import time
import gc
import ntptime
import urequests as requests

from math import floor
from bme680 import *
from blinker import blink
import config_reader as configReader

try:
  print('Setup BME680')
  i2c = machine.SoftI2C(scl=machine.Pin(5), sda=machine.Pin(4), timeout=5000)
  bme = BME680_I2C(i2c=i2c, address=119)
except Exception as e:
  print('Failed to setup BME680:', e)
  blink(6, 0.250)
  time.sleep(10)
  print('Resetting')
  machine.reset()

DELAY = 5 # delay between reads in minutes
UNIX_EPOCH= 946684800
syncronized= False
payload={}

payload['device_id'] = MAC

def syncTime(retry=0):
  try:
    print('Syncing time')
    ntptime.settime()
    return True
  except Exception as e:  
        print('Failed to sync time: retry in 10s', e)
        if retry < 3:
          time.sleep(10)
          syncTime(retry= retry+1)
        else:
          print('Failed to sync time after 3 tries. Check Network! Resetting...')
          blink(5, 0.250)
          machine.reset()
  finally:
    gc.collect()

def readSensors(retry=0, payload={}):
  try:
    
    payload['temperature'] = str(bme.temperature)
    payload['humidity'] = str(bme.humidity)
    payload['pressure'] = str(bme.pressure)
    payload['gas'] = str(bme.gas)
    payload['timestamp'] = str(time.time()+UNIX_EPOCH)

    print('Temperature:', payload['temperature'] + ' C')
    print('Humidity:', payload['humidity'] + ' %')
    print('Pressure:', payload['pressure'] + ' hPa')
    print('Gas:', payload['gas'] + ' Ohms')
    print('-------')    
    return payload

  except OSError as e:
    print('Failed to read sensor.', e)
    blink(7, 0.250)
    if retry < 3:
      time.sleep(10)
      readSensors(retry= retry+1, payload= payload)
    else:
        print('Failed to read sensors after 3 tries. Check wires! Resetting...')
        machine.reset()
  finally:
    gc.collect()

def getWaitTime(minutes):
  seconds= time.localtime()[5]
  waitTime = (DELAY-(minutes-(floor(minutes/DELAY)*DELAY)))*60000 - seconds*1000
  print('Waiting for: ', waitTime)
  return waitTime

def sendPayload(payload):
  try:
    requests.post(config['SERVER_URL'], json=payload)
  except Exception as e:
    print('Failed to post data to server!: ', e)



try:
    config = configReader.readConfig()
    ntptime.host= config['NTP_SERVER']
except Exception as e:
    print('Failed to read config!', e)
    blink(3, 0.250)
while True:
  hour= time.localtime()[3]
  minutes= time.localtime()[4]
  if not syncronized or (hour in [6,12,18,0] and minutes == 0):
    syncronized= syncTime()
    print("Time syncronized",str(time.localtime()))
  machine.lightsleep(getWaitTime(minutes))
  payload= readSensors(payload= payload)
  sendPayload(payload)
  gc.collect()