import json
def readConfig():
    with open('config.json') as json_file:
        config = json.load(json_file)
        return config